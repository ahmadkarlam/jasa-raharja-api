<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name'      => 'Ahmad Karlam',
            'email'     => 'ahmad.karlam@gmail.com',
            'password'  => 'secret',
        ]);
    }
}