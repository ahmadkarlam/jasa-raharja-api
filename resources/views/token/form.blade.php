@include('errors.validation')

@if (isset($model))
    {!! Form::model($model, ['method' => 'PUT', 'route' => ['token.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['route' => 'token.store', 'class' => 'form-horizontal']) !!}
@endif

    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

    <div class="form-group">
        <label class="col-md-4 control-label" for="name">Name</label>
        <div class="col-md-6">
            {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                Submit
            </button>
        </div>
    </div>
{!! Form::close() !!}