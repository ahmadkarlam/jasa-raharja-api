@extends('app')

@section('content')


    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new token</div>
                    <div class="panel-body">
                        @include('token.form')
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop