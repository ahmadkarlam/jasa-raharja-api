@extends('app')

@section('content')


    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">List Available Token</div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td class="text-center">#</td>
                                <td>Name</td>
                                <td>Token</td>
                                <td>Status</td>
                                <td class="text-center">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tokens as $key => $token)
                            <tr>
                                <td class="text-center">{!! ($key + 1) !!}</td>
                                <td>{!! $token->name !!}</td>
                                <td>{!! $token->token !!}</td>
                                <td>
                                    @if ($token->status)
                                        On
                                    @else
                                        Off
                                    @endif
                                </td>
                                <td class="text-center">
                                    <div class="btn-group" role="group" aria-label="action">
                                        <a href="{!! route('token.show', $token->id) !!}" class="btn btn-default">
                                            Detail
                                        </a>
                                        <a data-toggle="modal" href="#delete{{ $token->id }}" class="btn btn-default">
                                            Delete
                                        </a>
                                        @include('modals.delete', [
                                            'url' => route('token.destroy', $token->id),
                                            'model' => $token
                                        ])
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div class="text-center">{!! $tokens->render() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop