<?php namespace App\Commands;

use App\Commands\Command;

use App\Token;
use Illuminate\Contracts\Bus\SelfHandling;

class GenerateTokenNumberAndSave extends Command implements SelfHandling
{

    protected $name;

    /**
     * Create a new command instance.
     *
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $token = $this->getToken();

        $this->createToken($token);
    }

    public function getToken()
    {
        $token = $this->generateToken();

        while ($this->checkToken($token)) {
            $token = $this->generateToken();
        }

        return $token;
    }

    public function generateToken()
    {
        return bcrypt(time() . str_random() . time());
    }

    public function checkToken($token)
    {
        return Token::checkToken($token);
    }

    public function createToken($token)
    {
        return Token::create([
            'name' => $this->name,
            'token' => $token,
            'status' => 0
        ]);
    }

}
