<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model {

	protected $fillable = ['name', 'token', 'status'];

    public static function checkToken($token)
    {
        if (self::where('token', $token)->count())
        {
            return true;
        }

        return false;
    }

}
