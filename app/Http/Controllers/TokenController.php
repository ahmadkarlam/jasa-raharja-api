<?php namespace App\Http\Controllers;
;
use App\Commands\GenerateTokenNumberAndSave;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateTokenRequest;
use App\Token;
use Illuminate\Http\Request;

class TokenController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $tokens = Token::latest()->paginate(10);

		return view('token.index', compact('tokens'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('token.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTokenRequest $request
     * @return Response
     */
	public function store(CreateTokenRequest $request)
	{
        $this->dispatch(new GenerateTokenNumberAndSave($request->get('name')));

        return redirect('token');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $token = Token::find($id);

		return view('token.show', compact('token'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
